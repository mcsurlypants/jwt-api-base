# README #

JWT Api Base is a simple wrapper class for handling JWT auth and endpoint requests.

# Configure #

Modify the first few lines to define the api base auth URL for your JWT-auth enabled API, if needed change the username and password parameters to mirror the username and password fields required by your API provider.

For example:
```php
define("URL_SUFFIX_AUTH", 'api-auth/');
define("PARAM_USERNAME", 'username');
define("PARAM_PASSWORD", 'password');
```
Changes to:
```php
define("URL_SUFFIX_AUTH", 'jwt-auth/');
define("PARAM_USERNAME", 'user');
define("PARAM_PASSWORD", 'pass');
```